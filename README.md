# User Profile Fetcher ...

... is a little utility to get user profiles from source FTP (as json-files) and upload it to target HTTP-endpoint.


## Installation

There is no installation steps needed. 

Just download builded tool for windows/linux from [here](https://gitlab.com/go-lab/user-profile-fetcher/-/releases) ...

... and fill config.yml file with actual values:
```yml
FTP_ADDRESS: 127.0.0.10:21
FTP_LOGIN: bob
FTP_PASSWORD: superbob
FTP_TIMEOUT: 5s
# root folder for files search
FTP_SEARCH_PATH: /
# regexp for filter files by path.
# named parameters will be extracted, you may use it in target path template
FTP_PATH_PATTERN: /users/(?P<userId>[0-9]+)/profile_(?P<version>[0-9]+).json

REST_API_KEY_TYPE: Token
REST_API_KEY: 0supersecretaccessapikey1234567890xxyyzz
REST_API_TIMEOUT: 5s
# target URL template.
# placeholders <paramName> will be replaced with values parsed from source path,
# or empty string if parameter with given name does not exists
REST_URL_TEMPLATE: http://localhost:8000/users/profiles/<userId>/<version>

MEDIATOR_STATUS_FILE: status.json
MEDIATOR_LOG_FILE: error-log.json
# count of concurrent 'threads' to fetch-upload profiles
MEDIATOR_CONCURRENCY: 4
```


## Usage

Run as CLI from cmd/terminal:
```
./upf-cli -c config.yml
```
![done-linux](images/done-linux.png)

Or double-click executable:

![progress-win](images/progress-win.png)

Errors:

![error-win](images/error-win.png)


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/go-lab/user-profile-fetcher/-/tags). 


## TODO

Improve:

* Add GUI tool using [Fyne project](https://fyne.io/)

## Developing

Prerequsites:

* [go 1.19+](https://go.dev/doc/install)
* [docker-ce, docker-compose](https://docs.docker.com/engine/install/)
* [pre-commit tool](https://pre-commit.com/#install)

Setup dev environment:

* clone repo and go to the project's root
* setup dev ftp and restapi:
    ```
    cp deploy/docker-compose.sample.yml docker-compose.yml
    docker-compose up -d
    ```
    ... and copy you test files into `./ftp_data/bob/` dir
* enable pre commit hooks:
    ```
    pre-commit install
    ```
* run tests:
    ```
    go test ./... -v
    ```
* setup and run tool:
    ```
    cp config/upf_cli/config.yml local.yml
    go run cmd/upf_cli/*.go -c local.yml
    ```
