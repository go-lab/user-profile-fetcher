package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	Listen     string
	APIKey     string
	APIKeyType string
)

func init() {
	flag.StringVar(&Listen, "l", "127.0.0.1:8000", "host:port to listen on")
	flag.StringVar(&APIKey, "k", "secretkey", "auth token")
	flag.StringVar(&APIKeyType, "t", "Token", "auth token type")
	flag.Parse()
}

func main() {

	router := echo.New()

	router.Use(
		middleware.Recover(),
		middleware.Logger(),
		middleware.KeyAuthWithConfig(middleware.KeyAuthConfig{
			AuthScheme: APIKeyType,
			Validator: func(auth string, c echo.Context) (bool, error) {
				return auth == APIKey, nil
			},
		}),
		// check content type of the request
		func(next echo.HandlerFunc) echo.HandlerFunc {
			return func(c echo.Context) error {
				if c.Request().Header.Get("Content-Type") != echo.MIMEApplicationJSON {
					return c.JSON(http.StatusBadRequest, map[string]string{
						"message": "invalid content type",
					})
				}
				return next(c)
			}
		},
	)

	router.POST("/users/profiles/:id/:version", func(c echo.Context) error {
		data := make(map[string]interface{})
		if err := c.Bind(&data); err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{
				"message": "cannot parse body",
				"error":   err.Error(),
			})
		}

		// emulate hard work))
		time.Sleep(1 * time.Second)
		return c.JSON(http.StatusCreated, data)
	})

	log.Fatal(router.Start(Listen))
}
