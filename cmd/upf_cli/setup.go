package main

import (
	"flag"
	"os"

	"github.com/rs/zerolog"
	ftpclient "gitlab.com/go-lab/user-profile-fetcher/adapters/ftp_client"
	restapi "gitlab.com/go-lab/user-profile-fetcher/adapters/rest_api"
	statusstorage "gitlab.com/go-lab/user-profile-fetcher/adapters/status_storage"
	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

const DefaultConfigFile = "config.yml"

var (
	ConfigFileName string
	Config         *FetchProfilesConfig
	FTPClient      *ftpclient.FTPClient
	RESTClient     *restapi.RESTAPIClient
	StatusStore    *statusstorage.StatusStorage
	DataMediator   *mediator.Mediator
	ErrorLog       *zerolog.Logger
	Spinner        *CLISpinner
)

func setupOrExit() {
	flag.StringVar(&ConfigFileName, "c", DefaultConfigFile, "path to config file")
	flag.Parse()

	Spinner = NewSpinner("initialization").Startf("...")
	config, err := GetConfig(ConfigFileName)
	if err != nil {
		Spinner.Failf("cannot get config")
		exit(err)
	}
	Config = config

	FTPClient = &ftpclient.FTPClient{Config: ftpclient.FTPConfig{
		Address:  Config.FTPAddress,
		Login:    Config.FTPLogin,
		Password: Config.FTPPassword,
		Timeout:  Config.FTPTimeout,
	}}

	RESTClient = restapi.NewRESTAPIClient(restapi.RESTAPIClientConfig{
		APIKey:     Config.RESTAPIKey,
		APIKeyType: Config.RESTAPIKeyType,
		Timeout:    Config.RESTAPITimeout,
	})

	StatusStore, err = statusstorage.NewStatusStorage(Config.MediatorStatusFile)
	if err != nil {
		Spinner.Failf("cannot restore status map")
		exit(err)
	}

	DataMediator, err = mediator.NewMediator(FTPClient, RESTClient, StatusStore)
	if err != nil {
		Spinner.Failf("cannot create mediator")
		exit(err)
	}

	errLogFile, err := os.Create(Config.MediatorLogFile)
	if err != nil {
		Spinner.Failf("cannot create error-log file")
		exit(err)
	}
	logger := zerolog.New(errLogFile).With().Timestamp().Logger().Level(zerolog.ErrorLevel)
	ErrorLog = &logger

	Spinner.Stopf("done")
}
