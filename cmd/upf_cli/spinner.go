package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/theckman/yacspin"
)

const (
	maxMessageTextLen = 50
	trimmedMsgDelim   = ">...<"
)

var defaultSpinnerConf = yacspin.Config{
	Frequency:         100 * time.Millisecond,
	ColorAll:          true,
	Colors:            []string{"fgYellow"},
	CharSet:           yacspin.CharSets[33],
	SuffixAutoColon:   false,
	StopCharacter:     SpinnerStopCharacter,
	StopColors:        []string{"fgGreen"},
	StopMessage:       "done",
	StopFailCharacter: SpinnerFailCharacter,
	StopFailColors:    []string{"fgRed"},
	StopFailMessage:   "failed",
}

// CLISpinner provides methods for working with CLI output
//
// Spinner represents a single output row like:
// <progress bar/success or fail symbol> <suffix>: <message>
type CLISpinner struct {
	spinner    *yacspin.Spinner
	colors     []string
	failColors []string
}

// NewSpinner returns new instance of spinner
func NewSpinner(suffix string) (spin *CLISpinner) {
	ys, _ := yacspin.New(defaultSpinnerConf)
	if len(suffix) > 0 {
		ys.Suffix(fmt.Sprintf(" %s ", suffix))
	} else {
		ys.Suffix(" ")
	}
	return &CLISpinner{
		spinner:    ys,
		colors:     defaultSpinnerConf.Colors,
		failColors: defaultSpinnerConf.StopFailColors,
	}
}

// Startf starts spinner animation with provided message
//
// NOTE: do not start a few instances at the same - it will override each other
func (s *CLISpinner) Startf(msg string, args ...interface{}) (spin *CLISpinner) {
	s.spinner.Message(fmt.Sprintf(msg, args...))
	_ = s.spinner.Start()
	return s
}

// Stopf stops animation with success symbol and provided message.
//
// Message can be formatted in Prinf manner
func (s *CLISpinner) Stopf(msg string, args ...interface{}) (spin *CLISpinner) {
	s.spinner.StopMessage(fmt.Sprintf(msg, args...))
	_ = s.spinner.Stop()
	return s
}

// Failf stops animation with fail symbol and provided message.
//
// Message can be formatted in Prinf manner
func (s *CLISpinner) Failf(msg string, args ...interface{}) (spin *CLISpinner) {
	s.spinner.StopFailMessage(fmt.Sprintf(msg, args...))
	_ = s.spinner.StopFail()
	return s
}

// Errf continues animation with new message, but in fail colours
//
// Message can be formatted in Prinf manner
func (s *CLISpinner) Errf(msg string, args ...interface{}) {
	_ = s.spinner.Pause()
	defer func() {
		_ = s.spinner.Unpause()
	}()
	_ = s.spinner.Colors(s.failColors...)
	s.updMessage(fmt.Sprintf(msg, args...))
}

// Msgf continues animation with new message, but in normal progress colours
//
// Message can be formatted in Prinf manner
func (s *CLISpinner) Msgf(msg string, args ...interface{}) {
	_ = s.spinner.Pause()
	defer func() {
		_ = s.spinner.Unpause()
	}()
	_ = s.spinner.Colors(s.colors...)
	s.updMessage(fmt.Sprintf(msg, args...))
}

func (s *CLISpinner) updMessage(msg string, args ...interface{}) {
	text := fmt.Sprintf(msg, args...)
	trimmed := trimMessage(text, maxMessageTextLen, trimmedMsgDelim)
	s.spinner.Message(trimmed)
}

func trimMessage(msg string, maxSymbols int, delim string) (trimmed string) {
	var msgSymbolsCount int
	for range msg {
		msgSymbolsCount++
	}
	if msgSymbolsCount <= maxSymbols {
		// no trim needed
		return msg
	}

	var delimSymbolsCount int
	for range delim {
		delimSymbolsCount++
	}
	headEnd := (maxSymbols - delimSymbolsCount) / 2
	tailStart := msgSymbolsCount - (maxSymbols-delimSymbolsCount)/2

	builder := strings.Builder{}
	hasDelim := false
	for i, s := range msg {
		if i < headEnd || i > tailStart {
			builder.WriteRune(s)
		} else if !hasDelim {
			builder.WriteString(delim)
			hasDelim = true
		}
	}
	return builder.String()
}
