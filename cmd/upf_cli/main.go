package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

func main() {
	serveCtx, serveCancel := context.WithCancel(context.Background())
	sigCtx, sigCancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)

	// serve fetch tasks in background
	done := make(chan error)
	go func() {
		setupOrExit()
		done <- serve(serveCtx)
	}()

	select {
	case err := <-done:
		exit(err)
	case <-sigCtx.Done():
		// user cancelled the operation
		Spinner.Failf("operation cancelled")
		sp := NewSpinner("trying to cancel requests").Startf("... press ctrl+C to force")
		serveCancel()
		sigCancel()
		// wait until tasks done
		<-done
		sp.Stopf("done")
		exit(nil)
	}
}

func serve(ctx context.Context) (err error) {
	Spinner = NewSpinner("searching files").Startf("...")
	profiles, err := DataMediator.SearchProfiles(ctx, Config.FTPSearchPath, Config.FTPPathPattern)
	if err != nil {
		Spinner.Failf("fails")
		return multierror.Append(err)
	}
	Spinner.Stopf("done: %d profiles found", len(profiles))

	Spinner = NewSpinner("processing profiles").Startf("...")

	progress := DataMediator.StartProcessFiles(ctx, profiles, Config.RESTURLTemplate, Config.MediatorConcurrency)
	var msg mediator.ProgressMessage
	for msg = range progress {
		if msg.Error != nil {
			Spinner.Errf(msg.Message)
			ErrorLog.Err(msg.Error).Msg(msg.Message)
		} else {
			Spinner.Msgf(msg.Message)
		}
	}
	// check last message
	if msg.Error != nil {
		err = msg.Error
		Spinner.Failf(msg.Message)
	} else {
		Spinner.Stopf(msg.Message)
	}

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	Spinner = NewSpinner("saving status").Startf("...")
	flushErr := StatusStore.Flush()
	if flushErr != nil {
		Spinner.Failf("cannot save status map")
		return flushErr
	}
	Spinner.Stopf("done")

	return err
}

// exit prints summary and call os.Exit(x)
func exit(err error) {
	if err != nil {
		NewSpinner("result").Startf("...").Failf("Ooops, smth goes wrong, call support: %s", err.Error())
		NewSpinner("").Startf("...").Failf("press 'Enter' to exit...")
		fmt.Scanln()
		os.Exit(1)
	}
	NewSpinner("").Startf("...").Stopf("press 'Enter' to exit...")
	fmt.Scanln()
}
