package main

import (
	"fmt"
	"os"
	"time"

	"github.com/go-yaml/yaml"
)

type FetchProfilesConfig struct {
	// source
	FTPAddress     string        `yaml:"FTP_ADDRESS"`
	FTPLogin       string        `yaml:"FTP_LOGIN"`
	FTPPassword    string        `yaml:"FTP_PASSWORD"`
	FTPTimeout     time.Duration `yaml:"FTP_TIMEOUT"`
	FTPSearchPath  string        `yaml:"FTP_SEARCH_PATH"`
	FTPPathPattern string        `yaml:"FTP_PATH_PATTERN"`
	// target
	RESTAPIKey      string        `yaml:"REST_API_KEY"`
	RESTAPIKeyType  string        `yaml:"REST_API_KEY_TYPE"`
	RESTAPITimeout  time.Duration `yaml:"REST_API_TIMEOUT"`
	RESTURLTemplate string        `yaml:"REST_URL_TEMPLATE"`
	// mediator
	MediatorStatusFile  string `yaml:"MEDIATOR_STATUS_FILE"`
	MediatorLogFile     string `yaml:"MEDIATOR_LOG_FILE"`
	MediatorConcurrency int    `yaml:"MEDIATOR_CONCURRENCY"`
}

var FetchProfilesConfigDefault = &FetchProfilesConfig{
	// source
	FTPAddress:     "localhost:21",
	FTPLogin:       "bob",
	FTPPassword:    "superbob",
	FTPTimeout:     5 * time.Second,
	FTPPathPattern: "/users/(?P<userId>[0-9]+)/profile_(?P<version>[0-9]+).json",
	// target
	RESTAPIKey:      "0d8ee0bbe4e76b541a75d1419831a507c8be475b",
	RESTAPIKeyType:  "Token",
	RESTAPITimeout:  5 * time.Second,
	RESTURLTemplate: "http://localhost:8000/users/profiles/<userId>/<version>",
	// mediator
	MediatorStatusFile:  "status.json",
	MediatorLogFile:     "error-log.json",
	MediatorConcurrency: 4,
}

func GetConfig(configFileName string) (config *FetchProfilesConfig, err error) {
	config = FetchProfilesConfigDefault
	configFile, err := os.Open(ConfigFileName)
	if err != nil {
		return config, err
	}
	defer configFile.Close()

	err = yaml.NewDecoder(configFile).Decode(config)
	if err != nil {
		return config, fmt.Errorf("cannot parse config: %w", err)
	}
	return config, err
}
