//go:build windows
// +build windows

package main

const (
	SpinnerStopCharacter = "V"
	SpinnerFailCharacter = "X"
)
