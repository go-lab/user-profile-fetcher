//go:build !windows
// +build !windows

package main

const (
	SpinnerStopCharacter = "✓"
	SpinnerFailCharacter = "✗"
)
