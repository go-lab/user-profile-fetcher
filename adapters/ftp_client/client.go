package ftpclient

import (
	"context"
	"io"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	"github.com/jlaffaye/ftp"
	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

var _ mediator.FileStorage = &FTPClient{}

// FTPConfig contains data for FTP connection
type FTPConfig struct {
	Address  string
	Login    string
	Password string
	Timeout  time.Duration
}

// FTPClient provides methods to fetch data from FTP server
type FTPClient struct {
	Config FTPConfig
}

// Walk tries to find files in 'catalog' and sub-catalogs
func (c *FTPClient) Walk(ctx context.Context, catalog string) (names []string, err error) {
	conn, err := c.connect(ctx)
	if err != nil {
		return names, err
	}
	defer c.close(conn)

	walker := conn.Walk(catalog)
	for walker.Next() {
		select {
		case <-ctx.Done():
			return names, ctx.Err()
		default:
			// just continue
		}
		entry := walker.Stat()
		if entry.Type == ftp.EntryTypeFile {
			names = append(names, walker.Path())
		}
	}
	return names, nil
}

// Fetch downloads file content
func (c *FTPClient) Fetch(ctx context.Context, name string) (data []byte, err error) {
	conn, err := c.connect(ctx)
	if err != nil {
		return data, err
	}
	defer c.close(conn)

	resp, err := conn.Retr(name)
	if err != nil {
		return data, err
	}
	return io.ReadAll(resp)
}

// connect creates FTP connection and login
func (c *FTPClient) connect(ctx context.Context) (conn *ftp.ServerConn, err error) {
	conn, err = ftp.Dial(c.Config.Address, ftp.DialWithTimeout(c.Config.Timeout), ftp.DialWithContext(ctx))
	if err != nil {
		return conn, err
	}
	err = conn.Login(c.Config.Login, c.Config.Password)
	if err != nil {
		return conn, err
	}
	return conn, nil
}

// close desconnects from server
func (c *FTPClient) close(conn *ftp.ServerConn) (err error) {
	if lErr := conn.Logout(); lErr != nil {
		err = multierror.Append(lErr)
	}
	if qErr := conn.Quit(); qErr != nil {
		err = multierror.Append(qErr)
	}
	return err
}
