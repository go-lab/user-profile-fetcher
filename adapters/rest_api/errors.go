package restapi

import (
	"fmt"
	"net/http"

	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

const (
	ErrorRespMessageField = "message"

	UserNotExistsMessage = "user does not exists"
)

func errorFromResponse(code int, body map[string]interface{}) (err error) {
	msg := body[ErrorRespMessageField]

	if code == http.StatusBadRequest && msg == UserNotExistsMessage {
		return mediator.ErrUserNotExists
	}

	if code >= http.StatusBadRequest && code < http.StatusInternalServerError {
		return multierror.Append(
			mediator.ErrBadRequest,
			fmt.Errorf("response code %d, detail: %+v", code, body),
		)
	}

	if code >= http.StatusInternalServerError {
		return multierror.Append(
			mediator.ErrServerError,
			fmt.Errorf("response code %d, detail: %+v", code, body),
		)
	}

	return nil
}
