package restapi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

var _ mediator.Loader = &RESTAPIClient{}

// RESTAPIClientConfig contains data for API connection
type RESTAPIClientConfig struct {
	APIKey     string
	APIKeyType string
	Timeout    time.Duration
}

// RESTAPIClient provides method to upload data to target server
type RESTAPIClient struct {
	config RESTAPIClientConfig
	client *http.Client
}

// NewRESTAPIClient creates a new ready to use client instance
func NewRESTAPIClient(config RESTAPIClientConfig) (client *RESTAPIClient) {
	httpClient := &http.Client{
		Timeout: config.Timeout,
	}
	return &RESTAPIClient{
		config: config,
		client: httpClient,
	}
}

// Load uploads data to target endpoint
func (c *RESTAPIClient) Load(ctx context.Context, url string, data []byte) (err error) {
	if err := json.Unmarshal(data, &map[string]interface{}{}); err != nil {
		// we actually don't need decoded json... need only check if decoding is possible
		return fmt.Errorf("data is not valid json-object: %w", err)
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("cannot create request: %w", err)
	}
	req.Header["Authorization"] = c.getAuthHeaders()
	req.Header["Content-Type"] = []string{"application/json"}

	resp, err := c.client.Do(req)
	if err != nil {
		return fmt.Errorf("DES API request fails: %w", err)
	}
	defer resp.Body.Close()
	body := make(map[string]interface{})
	_ = json.NewDecoder(resp.Body).Decode(&body)

	return errorFromResponse(resp.StatusCode, body)
}

func (c *RESTAPIClient) getAuthHeaders() []string {
	authToken := fmt.Sprintf("%s %s", c.config.APIKeyType, c.config.APIKey)
	return []string{authToken}
}
