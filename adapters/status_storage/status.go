package statusstorage

import (
	"sync"

	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

var _ mediator.StatusStorage = &StatusStorage{}

type statusMap map[string]string

// StatusStorage is a simple implementation of key-val storage:
//   - keys and values must be strings
//   - implements mediator.StatusStorage interface for working with data
//   - thread-save (using mutex)
//   - working copy stores in-memory as map
//   - restores data from disk on initialization, flushes in Close() call
type StatusStorage struct {
	fileName  string
	statusMap statusMap
	mx        sync.RWMutex
}

// NewStatusStorage tries to load data from file and return ready to use instance of storage
func NewStatusStorage(fileName string) (status *StatusStorage, err error) {
	statusMap, err := restoreStatus(fileName)
	if err != nil {
		return status, err
	}

	return &StatusStorage{
		fileName:  fileName,
		statusMap: statusMap,
	}, nil
}

// Get returns value by provided key and 'true' as second param.
//
// If key does not exists returns empty string and 'false' as second param
func (ss *StatusStorage) Get(key string) (value string, ok bool) {
	ss.mx.RLock()
	defer ss.mx.RUnlock()
	value, ok = ss.statusMap[key]
	return value, ok
}

// Set updates value of key, ovverides if already exits. Always returns 'nil' error
func (ss *StatusStorage) Set(key, value string) (err error) {
	ss.mx.Lock()
	defer ss.mx.Unlock()
	ss.statusMap[key] = value
	return nil
}

// Flush saves data to file
func (ss *StatusStorage) Flush() (err error) {
	ss.mx.Lock()
	defer ss.mx.Unlock()
	return saveStatus(ss.statusMap, ss.fileName)
}
