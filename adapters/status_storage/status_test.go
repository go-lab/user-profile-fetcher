package statusstorage_test

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	statusstorage "gitlab.com/go-lab/user-profile-fetcher/adapters/status_storage"
)

func TestStatusStorageSetGet(t *testing.T) {
	statusFileName := path.Join(t.TempDir(), "status.json")
	status, err := statusstorage.NewStatusStorage(statusFileName)
	require.Nil(t, err)

	val, ok := status.Get("somekey")
	assert.Empty(t, val)
	assert.False(t, ok)

	err = status.Set("somekey", "someval")
	assert.Nil(t, err)

	val, ok = status.Get("somekey")
	assert.Equal(t, val, "someval")
	assert.True(t, ok)
}

func TestStatusStorageFlushRestore(t *testing.T) {
	statusFileName := path.Join(t.TempDir(), "status.json")

	t.Run("set and no flush", func(t *testing.T) {
		status, err := statusstorage.NewStatusStorage(statusFileName)
		require.Nil(t, err)

		val, ok := status.Get("somekey")
		assert.Empty(t, val)
		assert.False(t, ok)

		err = status.Set("somekey", "someval")
		assert.Nil(t, err)
	})

	t.Run("restore and get fails", func(t *testing.T) {
		status, err := statusstorage.NewStatusStorage(statusFileName)
		require.Nil(t, err)

		val, ok := status.Get("somekey")
		assert.Empty(t, val)
		assert.False(t, ok)
	})

	t.Run("set and flush", func(t *testing.T) {
		status, err := statusstorage.NewStatusStorage(statusFileName)
		require.Nil(t, err)

		val, ok := status.Get("somekey")
		assert.Empty(t, val)
		assert.False(t, ok)

		err = status.Set("somekey", "someval")
		assert.Nil(t, err)

		err = status.Flush()
		assert.Nil(t, err)
	})

	t.Run("restore and get", func(t *testing.T) {
		status, err := statusstorage.NewStatusStorage(statusFileName)
		require.Nil(t, err)

		val, ok := status.Get("somekey")
		assert.Equal(t, val, "someval")
		assert.True(t, ok)
	})
}

func TestStatusStorageRestoreFromOldFormat(t *testing.T) {
	statusFileName := path.Join(t.TempDir(), "status.json")

	// write old format: single json-object
	err := os.WriteFile(statusFileName, []byte("{\"key1\":\"val1\",\"key2\":\"val2\"}"), 0660)
	assert.Nil(t, err)

	status, err := statusstorage.NewStatusStorage(statusFileName)
	require.Nil(t, err)

	val, ok := status.Get("key1")
	assert.Equal(t, val, "val1")
	assert.True(t, ok)

	val, ok = status.Get("key2")
	assert.Equal(t, val, "val2")
	assert.True(t, ok)
}

func TestStatusStorageRestoreFail(t *testing.T) {
	statusFileName := path.Join(t.TempDir(), "status.json")

	// write non-valid data
	err := os.WriteFile(statusFileName, []byte("{"), 0660)
	assert.Nil(t, err)

	_, err = statusstorage.NewStatusStorage(statusFileName)
	assert.Error(t, err)
}
