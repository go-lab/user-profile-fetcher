package statusstorage

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
)

func restoreStatus(statusFilePath string) (status statusMap, err error) {
	status = statusMap{}

	if _, err := os.Stat(statusFilePath); os.IsNotExist(err) {
		// return empty status map - it is ok
		return status, nil
	}

	statusFd, err := os.Open(statusFilePath)
	if err != nil {
		return status, fmt.Errorf("status file '%s' creation fails: %w", statusFilePath, err)
	}
	defer statusFd.Close()

	scanner := bufio.NewScanner(statusFd)
	for scanner.Scan() {
		err = json.Unmarshal(scanner.Bytes(), &status)
		if err != nil {
			return status, fmt.Errorf("status file '%s' decoding fails: %w", statusFilePath, err)
		}
	}
	return status, nil
}

func saveStatus(status statusMap, statusFilePath string) (err error) {
	statusFd, err := os.Create(statusFilePath)
	if err != nil {
		return fmt.Errorf("status file '%s' opening fails: %w", statusFilePath, err)
	}
	defer statusFd.Close()

	encoder := json.NewEncoder(statusFd)
	for k, v := range status {
		err = encoder.Encode(map[string]string{k: v})
		if err != nil {
			return fmt.Errorf("status file '%s' encoding fails: %w", statusFilePath, err)
		}
	}
	return nil
}
