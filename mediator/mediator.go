package mediator

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

// Mediator provides method to transfer data from srt to trg
type Mediator struct {
	source    FileStorage
	target    Loader
	statusMap StatusStorage
}

// NewMediator returns new ready to use instance
func NewMediator(source FileStorage, target Loader, status StatusStorage) (md *Mediator, err error) {
	return &Mediator{
		source:    source,
		target:    target,
		statusMap: status,
	}, nil
}

// SearchProfiles searches files which name matches 'pattern' on source storage in provided 'catalog' and sub-catalogs
func (m *Mediator) SearchProfiles(ctx context.Context, catalog, pattern string) (profiles []*UserProfile, err error) {
	nameParser, err := NewUserProfileNameParser(pattern)
	if err != nil {
		return profiles, err
	}
	fileNames, err := m.source.Walk(ctx, catalog)
	if err != nil {
		return profiles, err
	}
	for _, name := range fileNames {
		nameParams, err := nameParser.Parse(name)
		if err != nil {
			continue // just skip non-matched
		}
		profiles = append(profiles, &UserProfile{name, nameParams})
	}
	return profiles, err
}

// StartProcessFiles runs file processing in coroutine.
//
// It takes json-files from the storage and puts them into the target system using Loader interface.
//
// Files, which path exists in current status map will be skipped.
// Paths of successfully uploaded files will be stored into status map to skip on next interation.
//
//	@param profiles: list of profiles to upload
//	@param uploadPathTemplate: template of target path to send profiles data
//	@param concurrency: is a number of workers started to process files
//	@returns progress: is a channel for progress messages.
//
// NOTE: Caller must read messages to prevent method blocking. Channel will be closed when all files will be processed
func (m *Mediator) StartProcessFiles(ctx context.Context, profiles []*UserProfile, uploadPathTemplate string, concurrency int) (progress chan ProgressMessage) {
	progress = make(chan ProgressMessage, concurrency)
	go m.processFiles(ctx, profiles, uploadPathTemplate, concurrency, progress)
	return progress
}

func (m *Mediator) processFiles(ctx context.Context, profiles []*UserProfile, uploadPathTemplate string, concurrency int, progress chan<- ProgressMessage) {
	// init report counters
	var skipped, failed atomic.Int32
	// init path builder
	uploadPathBuilder := NewPathBuilder(uploadPathTemplate)

	// init worker pool
	wpoolInput := make(chan *UserProfile, concurrency)
	wpoolWG := sync.WaitGroup{}
	for i := 0; i < concurrency; i++ {
		wpoolWG.Add(1)
		go func() {
			defer wpoolWG.Done()

			for profile := range wpoolInput {
				skip, err := m.handleProfile(ctx, profile.SourcePath, uploadPathBuilder.WithParams(profile.Params))
				switch true {
				case skip:
					skipped.Add(1)
					progress <- ProgressMessage{fmt.Sprintf("skip %s", profile.SourcePath), true, nil}
				case err != nil:
					failed.Add(1)
					progress <- ProgressMessage{fmt.Sprintf("failed %s", profile.SourcePath), false, err}
				default:
					progress <- ProgressMessage{fmt.Sprintf("success %s", profile.SourcePath), false, nil}
				}
			}
		}()
	}

	// produce tasks
	cancelled := false
	for _, profile := range profiles {
		if cancelled = IsContextDone(ctx); cancelled {
			break
		}
		wpoolInput <- profile
	}
	close(wpoolInput)

	// wait until all tasks done
	wpoolWG.Wait()

	// finish
	msg := fmt.Sprintf("done: %d files total, %d skipped, %d failed", len(profiles), skipped.Load(), failed.Load())
	if failed.Load() > 0 {
		progress <- ProgressMessage{msg, false, fmt.Errorf("some files processing fails, see error log")}
	} else {
		progress <- ProgressMessage{msg, false, nil}
	}
	close(progress)
}

func (m *Mediator) handleProfile(ctx context.Context, sourcePath, targetPath string) (skip bool, err error) {

	if _, ok := m.statusMap.Get(sourcePath); ok {
		// already processed
		return true, err
	}

	// fetch from sourse
	fileContent, err := m.source.Fetch(ctx, sourcePath)
	if err != nil {
		return false, fmt.Errorf("cannot fetch file: %w", err)
	}

	// upload to target
	err = m.target.Load(ctx, targetPath, fileContent)
	if errors.Is(err, ErrUserNotExists) {
		// it is not an error, skip and try later
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("cannot upload file: %w", err)
	}

	// update status
	err = m.statusMap.Set(sourcePath, time.Now().Format(time.RFC3339))

	return false, err
}

// IsContextDone is helper for non-blocking check if context Done() chan closed
func IsContextDone(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		// caller canceled the request
		return true
	default:
		return false
	}
}
