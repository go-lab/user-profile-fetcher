package mediator_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

var (
	_ mediator.FileStorage   = &FileStorageMock{}
	_ mediator.Loader        = &LoaderMock{}
	_ mediator.StatusStorage = &StatusStorageMock{}
)

// FileStorageMock implements mediator.FileStorage for test purposes
type FileStorageMock struct {
	mock.Mock
}

func (m *FileStorageMock) Walk(ctx context.Context, catalog string) (names []string, err error) {
	args := m.Called(ctx, catalog)
	return args.Get(0).([]string), args.Error(1)
}

func (m *FileStorageMock) Fetch(ctx context.Context, name string) (data []byte, err error) {
	args := m.Called(ctx, name)
	return args.Get(0).([]byte), args.Error(1)
}

// LoaderMock implements mediator.LoaderMock for test purposes
type LoaderMock struct {
	mock.Mock
}

func (m *LoaderMock) Load(ctx context.Context, path string, data []byte) (err error) {
	args := m.Called(ctx, path, data)
	return args.Error(0)
}

// StatusStorageMock implements mediator.StatusStorage for test purposes
type StatusStorageMock struct {
	mock.Mock
}

func (m *StatusStorageMock) Get(key string) (value string, ok bool) {
	args := m.Called(key)
	return args.String(0), args.Bool(1)
}

func (m *StatusStorageMock) Set(key, value string) (err error) {
	args := m.Called(key, value)
	return args.Error(0)
}

func setUpMock(t *testing.T) (*FileStorageMock, *LoaderMock, *StatusStorageMock, *mediator.Mediator) {
	source := new(FileStorageMock)
	target := new(LoaderMock)
	status := new(StatusStorageMock)

	datamediator, err := mediator.NewMediator(source, target, status)

	require.Nil(t, err, "cannot create mediator instance")

	return source, target, status, datamediator
}
