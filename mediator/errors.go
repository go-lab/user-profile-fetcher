package mediator

import (
	"errors"
	"fmt"
)

var (
	ErrApiCall       = errors.New("request fails")
	ErrUserNotExists = fmt.Errorf("user does not exists, %w", ErrApiCall)
	ErrBadRequest    = fmt.Errorf("client error, %w", ErrApiCall)
	ErrServerError   = fmt.Errorf("server error, %w", ErrApiCall)
)
