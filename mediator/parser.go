package mediator

import (
	"fmt"
	"regexp"
	"strings"
)

var pathParamRe = regexp.MustCompile("<([A-Za-z0-9-_]+)>")

// UserProfileNameParser provides Parse method for working with file names
type UserProfileNameParser struct {
	fileNameRe *regexp.Regexp
}

// NewUserProfileNameParser initiates internal regexp to speepup parsing
func NewUserProfileNameParser(pattern string) (parser *UserProfileNameParser, err error) {
	fileNameRe, err := regexp.Compile(pattern)
	if err != nil {
		return nil, fmt.Errorf("bad filename regexp pattern: %w", err)
	}
	return &UserProfileNameParser{fileNameRe: fileNameRe}, nil
}

// Parse try to split provided name into parts
func (p *UserProfileNameParser) Parse(name string) (nameObj UserProfileParamsMap, err error) {
	nameParts := p.fileNameRe.FindStringSubmatch(name)
	if nameParts == nil {
		return nil, fmt.Errorf("name does not match pattern")
	}

	result := make(UserProfileParamsMap, len(nameParts)-1) // excluding nameParts[0] - string itself
	for i := 1; i < len(nameParts); i++ {
		paramKey := p.fileNameRe.SubexpNames()[i]
		result[paramKey] = nameParts[i]
	}
	return result, nil
}

// PathBuilder is a tool to create target path
type PathBuilder struct {
	pathTemplate             string
	placeholderToParamKeyMap map[string]string
}

// NewPathBuilder parses template and returns ready to use instance of builder
func NewPathBuilder(pathTemplate string) (builder *PathBuilder) {
	placeholderToParamKeyMap := make(map[string]string)
	for _, match := range pathParamRe.FindAllStringSubmatch(pathTemplate, -1) {
		placeholder := match[0]
		paramKey := match[1]
		placeholderToParamKeyMap[placeholder] = paramKey
	}
	return &PathBuilder{pathTemplate, placeholderToParamKeyMap}
}

// WithParams creates path by replacing template placeholders with values from params map
func (b *PathBuilder) WithParams(params UserProfileParamsMap) (path string) {
	path = b.pathTemplate
	for placeholder, paramKey := range b.placeholderToParamKeyMap {
		paramValue := params[paramKey] // if param not exists - replace with empty string - it is ok
		path = strings.ReplaceAll(path, placeholder, paramValue)
	}
	return path
}
