package mediator_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

type parseTestRecord struct {
	testname        string
	expectedUserId  string
	expectedVersion string
	expectError     bool
}

func TestUserProfileNameParserParse(t *testing.T) {
	testTable := []parseTestRecord{
		{"/2022-01-02/100000/request_01_1664565589.json", "100000", "1664565589", false},
		{"/2022-01-02/100001/request_01_1664702789.json", "100001", "1664702789", false},
		{"/2022-01-02/100002/request_01_1664567149.json", "100002", "1664567149", false},
		{"/2022-01-02/100003/request_01.json", "0", "0", true},
		{"/2022-01-02/100004/request_01_aaa.json", "0", "0", true},
		{"/2022-01-02/aaaaaa/request_01.json", "0", "0", true},
	}

	parser, err := mediator.NewUserProfileNameParser("^/[0-9-]+/(?P<userId>[0-9]+)/request_[0-9]+_(?P<version>[0-9]+).json$")
	if err != nil {
		t.Fatalf("cannot create parser: %s", err.Error())
	}

	for i, testRow := range testTable {
		got, gotErr := parser.Parse(testRow.testname)

		hasError := gotErr != nil
		assert.Equal(t, hasError, testRow.expectError, "step %d method has error", i)
		if got != nil {
			assert.Equal(t, testRow.expectedUserId, got["userId"])
			assert.Equal(t, testRow.expectedVersion, got["version"])
		}
	}
}

type buildTestRecord struct {
	template     string
	params       mediator.UserProfileParamsMap
	expectedPath string
}

func TestPathBuilderWithParams(t *testing.T) {
	testTable := []buildTestRecord{
		{"/prefix/<userId>/<version>/suffix", mediator.UserProfileParamsMap{"userId": "100", "version": "X1"}, "/prefix/100/X1/suffix"},
		{"/prefix/<userId>/<version>/suffix", mediator.UserProfileParamsMap{"userId": "100"}, "/prefix/100//suffix"},
		{"/prefix/<userId>/suffix", mediator.UserProfileParamsMap{"userId": "100", "version": "X1"}, "/prefix/100/suffix"},
	}

	for i, testRow := range testTable {
		builder := mediator.NewPathBuilder(testRow.template)
		gotPath := builder.WithParams(testRow.params)
		assert.Equal(t, testRow.expectedPath, gotPath, "step %d invalid path", i)
	}
}
