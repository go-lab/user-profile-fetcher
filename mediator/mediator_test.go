package mediator_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/go-lab/user-profile-fetcher/mediator"
)

func TestMediatorSourceWalk(t *testing.T) {

	t.Run("success", func(t *testing.T) {
		source, _, _, datamediator := setUpMock(t)
		call := source.
			On("Walk", mock.Anything, "/").
			Return([]string{"/100/x.json", "/200/y.json", "/300/z.txt"}, nil).
			Times(1)

		gotProfiles, gotErr := datamediator.SearchProfiles(context.Background(), "/", "/(?P<id>[0-9]+)/.+.json")

		call.Parent.AssertExpectations(t)
		assert.NoError(t, gotErr, "unexpected error")
		assert.Equal(t, []*mediator.UserProfile{
			{"/100/x.json", mediator.UserProfileParamsMap{"id": "100"}},
			{"/200/y.json", mediator.UserProfileParamsMap{"id": "200"}},
		}, gotProfiles)
	})

	t.Run("fails", func(t *testing.T) {
		source, _, _, datamediator := setUpMock(t)

		var testNames []string
		call := source.
			On("Walk", mock.Anything, "/").
			Return(testNames, fmt.Errorf("test err")).
			Times(1)

		gotNames, gotErr := datamediator.SearchProfiles(context.Background(), "/", "/(?P<id>[0-9]+)/.+.json")

		call.Parent.AssertExpectations(t)
		assert.Error(t, gotErr, "expected error")
		assert.Nil(t, gotNames)
	})

}

func TestMediatorStartProcessFiles(t *testing.T) {

	var testProfiles = []*mediator.UserProfile{
		{"/prefix/930558/request_1664565589.json", mediator.UserProfileParamsMap{"id": "930558", "ver": "1664565589"}},
		{"/prefix/930295/request_1664702789.json", mediator.UserProfileParamsMap{"id": "930295", "ver": "1664702789"}},
	}
	var testTargetPaths = []string{
		"/u/930558/1664565589/p",
		"/u/930295/1664702789/p",
	}
	var testTargetPathTemplate = "/u/<id>/<ver>/p"

	t.Run("success", func(t *testing.T) {
		source, target, status, datamediator := setUpMock(t)

		for i, profile := range testProfiles {
			source.On("Fetch", mock.Anything, profile.SourcePath).Return([]byte("{}"), nil)
			target.On("Load", mock.Anything, testTargetPaths[i], mock.Anything).Return(nil)
		}
		status.On("Set", mock.Anything, mock.Anything).Return(nil).Times(len(testProfiles))
		status.On("Get", mock.Anything).Return("", false).Times(len(testProfiles))

		messages := datamediator.StartProcessFiles(context.Background(), testProfiles, testTargetPathTemplate, 1)
		for msg := range messages {
			assert.Nil(t, msg.Error, "unexpected error")
		}
		for _, profile := range testProfiles {
			source.AssertCalled(t, "Fetch", mock.Anything, profile.SourcePath)
		}
		target.AssertExpectations(t)
		status.AssertExpectations(t)
	})

	t.Run("fails cannot fetch", func(t *testing.T) {
		source, target, status, datamediator := setUpMock(t)

		source.On("Fetch", mock.Anything, mock.Anything).Return(*new([]byte), fmt.Errorf("test error"))
		target.On("Load", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		status.On("Set", mock.Anything, mock.Anything).Return(nil)
		status.On("Get", mock.Anything).Return("", false)

		messages := datamediator.StartProcessFiles(context.Background(), testProfiles, testTargetPathTemplate, 1)
		for msg := range messages {
			assert.Error(t, msg.Error)
		}
		source.AssertNumberOfCalls(t, "Fetch", len(testProfiles))
		target.AssertNotCalled(t, "Load")
		status.AssertNotCalled(t, "Set")
		status.AssertNumberOfCalls(t, "Get", len(testProfiles))
	})

	t.Run("fails cannot upload", func(t *testing.T) {
		source, target, status, datamediator := setUpMock(t)

		source.On("Fetch", mock.Anything, mock.Anything).Return([]byte("{}"), nil)
		target.On("Load", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("test error"))
		status.On("Set", mock.Anything, mock.Anything).Return(nil)
		status.On("Get", mock.Anything).Return("", false)

		messages := datamediator.StartProcessFiles(context.Background(), testProfiles, testTargetPathTemplate, 1)
		for msg := range messages {
			assert.Error(t, msg.Error)
		}
		source.AssertNumberOfCalls(t, "Fetch", len(testProfiles))
		target.AssertNumberOfCalls(t, "Load", len(testProfiles))
		status.AssertNotCalled(t, "Set")
		status.AssertNumberOfCalls(t, "Get", len(testProfiles))
	})

	t.Run("skip duplicate", func(t *testing.T) {
		source, target, status, datamediator := setUpMock(t)

		source.On("Fetch", mock.Anything, mock.Anything).Return([]byte("{}"), nil)
		target.On("Load", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
		status.On("Set", mock.Anything, mock.Anything).Return(nil)
		status.On("Get", mock.Anything).Return("xxx", true)

		messages := datamediator.StartProcessFiles(context.Background(), testProfiles, testTargetPathTemplate, 1)
		skipped := 0
		for msg := range messages {
			assert.Nil(t, msg.Error)
			if msg.Skipped {
				skipped++
			}
		}
		source.AssertNotCalled(t, "Fetch")
		target.AssertNotCalled(t, "Load")
		status.AssertNotCalled(t, "Set")
		status.AssertNumberOfCalls(t, "Get", len(testProfiles))
		assert.Equal(t, len(testProfiles), skipped)
	})

	t.Run("skip user not exists", func(t *testing.T) {
		source, target, status, datamediator := setUpMock(t)

		source.On("Fetch", mock.Anything, mock.Anything).Return([]byte("{\"key\":\"val\"}"), nil)
		target.On("Load", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mediator.ErrUserNotExists)
		status.On("Set", mock.Anything, mock.Anything).Return(nil)
		status.On("Get", mock.Anything).Return("", false)

		messages := datamediator.StartProcessFiles(context.Background(), testProfiles, testTargetPathTemplate, 1)
		skipped := 0
		for msg := range messages {
			assert.Nil(t, msg.Error)
			if msg.Skipped {
				skipped++
			}
		}
		source.AssertNumberOfCalls(t, "Fetch", len(testProfiles))
		target.AssertNumberOfCalls(t, "Load", len(testProfiles))
		status.AssertNotCalled(t, "Set")
		status.AssertNumberOfCalls(t, "Get", len(testProfiles))
		assert.Equal(t, len(testProfiles), skipped)
	})

	t.Run("cancel", func(t *testing.T) {
		source, target, status, datamediator := setUpMock(t)

		source.On("Fetch", mock.Anything, mock.Anything).Return([]byte("{\"key\":\"val\"}"), nil)
		target.On("Load", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
		status.On("Set", mock.Anything, mock.Anything).Return(nil)
		status.On("Get", mock.Anything).Return("", false)

		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		messages := datamediator.StartProcessFiles(ctx, testProfiles, testTargetPathTemplate, 1)
		for msg := range messages {
			assert.Nil(t, msg.Error)
		}
		source.AssertNotCalled(t, "Fetch")
		target.AssertNotCalled(t, "Load")
		status.AssertNotCalled(t, "Set")
		status.AssertNotCalled(t, "Get")
	})

}
