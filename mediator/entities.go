package mediator

import (
	"context"
)

// FileStorage is interface of source data storage
type FileStorage interface {
	Walk(ctx context.Context, catalog string) (names []string, err error)
	Fetch(ctx context.Context, name string) (data []byte, err error)
}

// Loader is interface of target server to send data
type Loader interface {
	Load(ctx context.Context, path string, data []byte) (err error)
}

// StatusStorage is interface of db to store data of already processed files
type StatusStorage interface {
	Get(key string) (value string, ok bool)
	Set(key, value string) (err error)
}

// ProgressMessage represents an event during file processing
type ProgressMessage struct {
	Message string `json:"message"`
	Skipped bool   `json:"skipped"`
	Error   error  `json:"error"`
}

// UserProfileParamsMap represents attributes that should be extracted from
// profile path in source system and passed to target path.
//
// For example:
//
//	source name regexp:
//	  "^/(?P<date>[0-9-]+)/(?P<userId>[0-9]+)/profile_(?P<version>[0-9]+).json$"
//	and target path:
//	  "https://example.com/users/profiles/<userId>/<version>/"
//
// NOTE: parameter names should contains only [A-Za-z0-9-_] symbols
type UserProfileParamsMap map[string]string

// UserProfile contains profile metadata needed for futher processing
type UserProfile struct {
	SourcePath string
	Params     UserProfileParamsMap
}
